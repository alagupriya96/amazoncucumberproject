package com.cts.automation.stepdefs;

import com.cts.automation.baseclass.BaseClass;
import com.cts.automation.pageobject.AmazonSearch;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDef extends BaseClass {


@Given("lanunches chrome and navigate to URL")
public void lanunches_chrome_and_navigate_to_URL() {
	 launchChrome();
	 driver.get("https://www.amazon.in/");
}

@When("user enters Search value as {string}.")
public void user_enters_Search_value_as(String string) {
	AmazonSearch search=new AmazonSearch();
	search.searchprdct(string);
}

@Then("Search should be sucessfull.")
public void search_should_be_sucessfull() {
    
}


}
