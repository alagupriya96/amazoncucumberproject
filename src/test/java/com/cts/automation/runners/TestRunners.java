package com.cts.automation.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( features="src//test//resources",dryRun=false,
glue="com.cts.automation.stepdefs" ,monochrome=true)
public class TestRunners {

}
