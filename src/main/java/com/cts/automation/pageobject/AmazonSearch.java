package com.cts.automation.pageobject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cts.automation.baseclass.BaseClass;

public class AmazonSearch extends BaseClass {

	
	public AmazonSearch()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//input[@id='twotabsearchtextbox']")
	public WebElement searchTxt;
	@FindBy(xpath="//input[@id='nav-search-submit-button']")
	public WebElement searchBtn;
	public void searchprdct(String prdct)
			
	{
		searchTxt.click();
		searchTxt.sendKeys(prdct);
		searchBtn.click();
		
	}
}
